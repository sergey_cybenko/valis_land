<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');
Route::get('/post/{slug}', 'MainController@article')->name('post');
Route::get('/success', 'MainController@success')->name('success');
Route::get('/license', 'MainController@license')->name('license');
Route::post('/', 'CustomerController@store_public')->name('customer.store.public');

Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {

    Route::get('article', 'ArticleController@index')->name('article.index');
    Route::post('article', 'ArticleController@store')->name('article.store');
    Route::get('article/create', 'ArticleController@create')->name('article.create');
    Route::get('article/{slug}', 'ArticleController@show')->name('article.show');
    Route::put('article/{slug}', 'ArticleController@update')->name('article.update');
    Route::delete('article/{slug}', 'ArticleController@destroy')->name('article.destroy');
    Route::get('article/{slug}/edit', 'ArticleController@edit')->name('article.edit');
    Route::post('article/{customer}', 'OrderController@store')->name('order.store');

    Route::resource('customer', 'CustomerController');

    Route::get('count/customer/default', 'CustomerController@count_default')->name('count.customer.default');

    Route::resource('status', 'StatusController', [
        'except' => ['edit']
    ]);

    Route::resource('product', 'ProductController', [
        'except' => ['edit']
    ]);

    Route::resource('construction', 'ConstructionController', [
        'except' => ['edit']
    ]);
    Route::resource('furniture', 'FurnitureController', [
        'except' => ['edit']
    ]);
    Route::resource('profile', 'ProfileController', [
        'except' => ['edit']
    ]);
    Route::resource('glass', 'GlassController', [
        'except' => ['edit']
    ]);
});
