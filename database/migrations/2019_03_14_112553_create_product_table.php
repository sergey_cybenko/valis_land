<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->text('title')->nullable();
			$table->text('description')->nullable();
			$table->integer('price')->nullable();
			$table->integer('show')->default(0);
            $table->integer('profile_id')->nullable();
            $table->integer('glass_id')->nullable();
            $table->integer('furniture_id')->nullable();
            $table->integer('construction_id')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}
