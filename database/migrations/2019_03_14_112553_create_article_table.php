<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('article', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->text('title')->nullable();
			$table->text('content')->nullable();
			$table->text('description')->nullable();
			$table->text('thumbnail')->nullable();
			$table->text('keywords')->nullable();
			$table->text('slug')->nullable();
            $table->integer('user_id');
            $table->integer('show')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('article');
	}

}
