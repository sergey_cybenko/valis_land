<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glass extends Model
{
    protected $table = 'glass';

    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The profiles that belong to the glasses.
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Profile', 'profile_glass');
    }
}
