<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';

    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The furnitures that belong to the profile.
     */
    public function furnitures()
    {
        return $this->belongsToMany('App\Furniture', 'profile_furniture');
    }

    /**
     * The glasses that belong to the profile.
     */
    public function glasses()
    {
        return $this->belongsToMany('App\Glass', 'profile_glass');
    }
}
