<?php

namespace App\Http\Controllers;

use App\Construction;
use App\Furniture;
use App\Glass;
use App\Product;
use App\Profile;
use App\Rules\ProfileDependencies;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Product::paginate(10);

        return view('admin.product.index', compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $constructions = Construction::all();
        $profiles = Profile::all();
        $furnitures = Furniture::all();
        $glasses = Glass::all();

        return view('admin.product.create',
            compact('constructions', 'profiles', 'furnitures', 'glasses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $request->validate([
            'profile' => 'required|numeric',
            'furniture' => ['required', new ProfileDependencies($data['profile'])],
            'glass' => ['required', new ProfileDependencies($data['profile'])],
        ]);

        $model = new Product($data);

        $model->construction()->associate(Construction::find($data['construction']));
        $model->profile()->associate(Profile::find($data['profile']));
        $model->furniture()->associate(Furniture::find($data['furniture']));
        $model->glass()->associate(Glass::find($data['glass']));

        $model->save();

        if ($model->title == null) {
            $model->title = $model->construction->name . ' ' .
                $model->profile->name . ' ' . $model->furniture->name . ' ' .
                $model->glass->name;
            $model->save();
        }

        if ($model->description == null) {
            $model->description = $model->construction->name . "\n" .
                'Профиль: ' . $model->profile->name . "\n" . 'Фурнитура: ' . $model->furniture->name . "\n" .
                'Стеклопакет: ' . $model->glass->name  . "\n" . 'Цена: ' . $model->price;
            $model->save();
        }

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $constructions = Construction::all();
        $profiles = Profile::all();
        $furnitures = Furniture::all();
        $glasses = Glass::all();

        return view('admin.product.show',
            compact('constructions', 'profiles', 'furnitures', 'glasses', 'product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $request->validate([
            'profile' => 'required|numeric',
            'furniture' => ['required', new ProfileDependencies($data['profile'])],
            'glass' => ['required', new ProfileDependencies($data['profile'])],
        ]);

        $model = Product::findOrFail($id);
        $model->update($data);

        $model->construction()->associate(Construction::find($data['construction']));
        $model->profile()->associate(Profile::find($data['profile']));
        $model->furniture()->associate(Furniture::find($data['furniture']));
        $model->glass()->associate(Glass::find($data['glass']));

        $model->save();

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();

        return redirect()->route('product.index');
    }
}
