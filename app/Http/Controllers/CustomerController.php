<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Status;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Customer::paginate(10);

        return view('admin.customer.index', compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();

        return view('admin.customer.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric|digits:10',
            'comment' => 'nullable',
            'status' => 'nullable'
        ],
            [
                'name.required' => 'Поле имени обязательно',
                'phone.required' => 'Поле номера телефона обязательно',
                'phone.numeric' => 'Поле номера телефона должно содержать только цифры',
                'phone.digits' => 'Поле номера телефона должно содержать 10 цифр',
            ]);

        $customer = new Customer($request->all());
        if (!array_key_exists('status', $data)) {
            $customer->status()->associate(Status::find(env('DEFAULT_STATUS_ID')));
        } else {
            $customer->status()->associate(Status::find($data['status']));
        }
        $customer->save();

        if (isset($data['comment'])) {
            $customer->orders()->create([
                'description' => $data['comment']
            ]);
        }

        if (!array_key_exists('status', $data)) {
            return redirect()->route('success');
        }
        return redirect()->route('customer.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_public(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric|digits:10',
            'comment' => 'nullable',
        ],
            [
                'name.required' => 'Поле имени обязательно',
                'phone.required' => 'Поле номера телефона обязательно',
                'phone.numeric' => 'Поле номера телефона должно содержать только цифры',
                'phone.digits' => 'Поле номера телефона должно содержать 10 цифр',
            ]);

        $customer = Customer::make($data);
        $customer->status_id = env('DEFAULT_STATUS_ID');

        $customer->save();

        if (isset($data['comment'])) {
            $customer->orders()->create([
                'description' => $data['comment']
            ]);
        }

        if (!array_key_exists('status', $data)) {
            return redirect()->route('success');
        }
        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Customer::findOrFail($id);
        $statuses = Status::all();

        return view('admin.customer.show', compact('model', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Customer::findOrFail($id);

        $model->update($request->all());

        $model->status()->dissociate();
        $model->status()->associate(Status::find($request->all()['status']));

        return redirect()->route('customer.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::findOrFail($id)->delete();

        return redirect()->route('customer.index');
    }

    /**
     * Show counting with default status customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function count_default()
    {
        $count = Customer::where('status_id', env('DEFAULT_STATUS_ID'))->count();

        return response($count, 200)
            ->header('Content-Type', 'text/plain');
    }


}
