<?php

namespace App\Http\Controllers;

use App\Article;
use App\Construction;
use Illuminate\Support\Facades\Log;
use PHPMailer\PHPMailer\PHPMailer;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Construction::all();

        return view('main', compact('collection'));
    }

    /**
     * Display the Accept of submit form.
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {
        return view('success');
    }

    /**
     * Display the license.
     *
     * @return \Illuminate\Http\Response
     */
    public function license()
    {
        return view('license');
    }

    /**
     * Display the article.
     *
     * @return \Illuminate\Http\Response
     */
    public function article($slug)
    {
        $article = Article::where('slug', '=', $slug)->where('show', 1)->firstOrFail();
        return view('article', compact('article'));
    }

    /**
     * @param $data
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function sendMail($data): void
    {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = env('MAIL_HOST');
            $mail->SMTPAuth = true;
            $mail->Username = env('MAIL_USERNAME');
            $mail->Password = env('MAIL_PASSWORD');
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            //Recipients
            $mail->setFrom(env('MAIL_USERNAME'), env('MAIL_NAME'));
            $mail->addAddress(env('MAIL_USERNAME'), env('MAIL_NAME'));

            //Content
            $mail->isHTML(true);
            $mail->Subject = iconv("windows-1251","utf-8", env('MAIL_TITLE'));
            $mail->Body = "{$data['name']}<br>{$data['phone']}<br>{$data['comment']}";

            $mail->send();
            Log::info("Mail send OK \n{$data['name']}\n{$data['phone']}\n{$data['comment']}\n");
        } catch (Exception $e) {
            Log::info("Error Mail send \n{$data['name']}\n{$data['phone']}\n{$data['comment']}\n");
        }
    }
}
