<?php

namespace App\Http\Controllers;

use App\Furniture;
use App\Glass;
use App\Profile;
use App\Rules\ProfileDependencies;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Profile::paginate(10);

        return view('admin.profile.index', compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $furnitures = Furniture::all();
        $glasses = Glass::all();

        return view('admin.profile.create',
            compact('furnitures', 'glasses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'furnitures' => 'required',
            'glasses' => 'required',
        ]);

        $data = $request->all();
        $model = new Profile($data);
        $model->save();

        foreach ($data['furnitures'] as $id) {
            $model->furnitures()->attach(Furniture::find($id));
        }
        foreach ($data['glasses'] as $id) {
            $model->glasses()->attach(Glass::find($id));
        }

        $model->save();

        return redirect()->route('profile.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        $furnitures = Furniture::all();
        $glasses = Glass::all();

        return view('admin.profile.show',
            compact('profile', 'furnitures', 'glasses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'furnitures' => ['required'],
            'glasses' => ['required'],
        ]);

        $data = $request->all();
        $model = Profile::findOrFail($id);
        $model->update($data);

        $model->furnitures()->detach($model->furnitures->pluck('id')->all());
        $model->glasses()->detach($model->glasses->pluck('id')->all());

        foreach ($data['furnitures'] as $id) {
            $model->furnitures()->attach(Furniture::find($id));
        }
        foreach ($data['glasses'] as $id) {
            $model->glasses()->attach(Glass::find($id));
        }

        $model->save();

        return redirect()->route('profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profile::findOrFail($id)->delete();

        return redirect()->route('profile.index');
    }
}
