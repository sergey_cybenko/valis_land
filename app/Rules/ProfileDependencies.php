<?php

namespace App\Rules;

use App\Profile;
use Illuminate\Contracts\Validation\Rule;

class ProfileDependencies implements Rule
{
    protected $profile_id;
    protected $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->profile_id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($attribute == 'furniture') {
            if (!in_array($value, (Profile::find($this->profile_id))->furnitures->pluck('id')->all())) {
                $this->message = 'Фурнитура несовместима с профилем!';
                return false;
            }
        }elseif ($attribute == 'glass') {
            if (!in_array($value, (Profile::find($this->profile_id))->glasses->pluck('id')->all())) {
                $this->message = 'Стеклопакет несовместим с профилем!';
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
