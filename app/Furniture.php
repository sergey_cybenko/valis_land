<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Furniture extends Model
{
    protected $table = 'furniture';

    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The profiles that belong to the furnitures.
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Profile', 'profile_furniture');
    }
}
