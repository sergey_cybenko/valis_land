<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Construction extends Model
{
    protected $table = 'construction';

    protected $fillable = [
        'name', 'description'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getImage()
    {
        if (!$this->image) {
            return "/uploads/avatars/no_avatar.png";
        }
        return "/uploads/avatars/$this->image";
    }
    public function uploadImage($image)
    {
        if($image == null) { return; }
        $this->removeImage();
        $fileName = Str::random(30). '.' . $image->extension();
        $image->move('uploads/avatars', $fileName);
        $this->image = $fileName;
        $this->save();
    }
    public function removeImage()
    {
        if($this->image != null) {
            Storage::delete('uploads/avatars/' . $this->image);
        }
    }
}
