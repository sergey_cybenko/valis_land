@extends('layouts.nav_app')

@section('meta-data')
    <title>{{ $article->title }}</title>
    <meta name="description" content="{{ $article->description }}">
    <meta name="keywords" content="{{ $article->keywords }}">
@endsection

@section('content')

    <main>
        <div class="container" id="our_jobs">
            <!--Section: Our Jobs-->
            <section class="pt-5" id="our-jobs">

                <h1 class="my-5 h1">{{ $article->title }}</h1>

                <!--First row-->
                <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                    <!--Grid column-->
                    <div class="col-xl-10 col-sm-12 my-3">

                        {!! $article->content !!}

                    </div>
                    <!--/Grid column-->

                </div>
                <!--/First row-->

            </section>
            <!--Section: More-->

            @include('layouts.order')

        </div>
    </main>

@endsection