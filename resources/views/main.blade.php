@extends('layouts.app')
@section('content')
<body>

<!-- data-ride="carousel" -->
<!--Carousel Wrapper-->
<div id="carousel-example-1z" class="carousel slide carousel-fade">

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <!--First slide-->
        <div class="carousel-item active" style="
            background-image: url(/img/main.jpg);
            background-size: cover;
        ">
            <div class="view">

                <!-- Mask & flexbox options-->
                <div class="mask d-flex justify-content-center align-items-center"
                    style="background-color: rgba(0,0,0,0.5);">

                    <!-- Content -->
                    <div class="text-center white-text mx-5 wow fadeIn"
                         style="visibility: visible; animation-name: fadeIn;">
                        <h1 class="mb-4">
                            <strong>Забудьте о сырости и холоде зимой</strong>
                        </h1>

                        <p style="font-size: 1.5rem;">
                            <strong>Новые окна Вас согреют</strong>
                        </p>

                        <a href="#order"
                           class="btn btn-outline-white btn-lg waves-effect waves-light">Вызвать замерщика
                        </a>
                    </div>
                    <!-- Content -->

                </div>
                <!-- Mask & flexbox options-->

            </div>
        </div>
        <!--/First slide-->

    </div>
    <!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->

<!--Main layout-->
<main>
    <div class="container">

        <!--Section: Main info-->
        <section class="mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

            <h2 class="h2 text-center mb-5">Окна и Балконы</h2>

            <!--First row-->
            <div class="row features-small my-5" style="visibility: visible; animation-name: fadeIn;">

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Надежная Фурнитура</h5>
                            <p class="grey-text mt-2">В окнах используется только самая качественная фурнитура FORNAX и ROTO, MAKO и KALE</p>
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Прочный Профиль</h5>
                            <p class="grey-text mt-2">Используются профиля A B класса, с пятью и тремя воздушными камерами соответственно, что гарантирует высокую прочность</p>
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Качественная Установка</h5>
                            <p class="grey-text mt-2">Установка пластиковых окон выполняется высококлассными специалистами по всем стандартам</p>
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

            </div>
            <!--/First row-->

            @foreach($collection as $construction)
            <!--Grid row-->
            <div class="row mb-5">

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <img src="{{ $construction->getImage() }}" class="img-fluid z-depth-1-half" alt="">

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <!-- Main heading -->
                    <h3 class="h3 mb-3 ">{{ $construction->name }}</h3>
                    <p>{{ $construction->description }}</p>
                    <hr>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Профиль</th>
                                <th class="furniture">Фурнитура</th>
                                <th>Стеклопакет</th>
                                <th>Стоимость</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($construction->products as $product)
                            <tr>
                                <th>{{ $product->profile->name }}</th>
                                <th class="furniture">{{ $product->furniture->name }}</th>
                                <th>{{ $product->glass->name }}</th>
                                <th>{{ $product->price }} руб.</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <p class="small grey-text">** Цены указаны на окно без учета откосов, поддоконника и отлива</p>

                    <!-- CTA -->
                    <span class="text-center d-inline-block">
                         <a href="#order"
                            class="btn btn-grey btn-md waves-effect waves-light">
                            Вызвать замерщика
                        </a>
                        <p class="small grey-text m-0">* Бесплатно</p>
                    </span>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->
            @endforeach

        </section>
        <!--Section: Main info-->

        <hr class="my-5">

        <!--Section: Not enough-->
        <section class="mb-5">

            <h2 class="my-5 h2 text-center">Фурнитура</h2>

            <!--First row-->
            <div class="row features-small mb-5 mt-3 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                <!--First column-->
                <div class="col-md-4">
                    <!--First row-->
                    <div class="row">
                        <div class="col-10 text-center">
                            <h4 class="h4 feature-title">ROTO</h4>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Немецкая фурнитура премиум класса</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Антикоррозийное покрытие</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->

                    <!--Fourth row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Усиленный поворотно-откидной механизм</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Fourth row-->

                    <!--Fourth row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Специальное самовосстанавливающееся покрытие</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Fourth row-->
                </div>
                <!--/First column-->

                <!--Second column-->
                <div class="col-md-4 flex-center">
                    <img src="/img/furnitura.jpg"
                         alt="Фурниатура для пластиковых окон" class="img-fluid z-depth-1-half">
                </div>
                <!--/Second column-->

                <!--Third column-->
                <div class="col-md-4 mt-2">
                    <!--First row-->
                    <div class="row">
                        <div class="col-10 text-center">
                            <h4 class="h4 feature-title">MAKO</h4>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Австийская фирма производитель фурнитуры. Является обладателем качества DIN ISO 9001
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Уникальное трехслойное покрытие от коррозии</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->

                    <!--Fourth row-->
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Все элементы фурнитуры изготовлены из литой или прессованной оцинкованной стали</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Fourth row-->
                </div>
                <!--/Third column-->

            </div>
            <!--/First row-->

        </section>
        <!--Section: Not enough-->

        <hr class="mb-5">

        <!--Section: Main features & Quick Start-->
        <section class="mb-5">

            <h2 class="h2 text-center mb-5">Стеклопакет</h2>

            <!--Grid row-->
            <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                <!--Grid column-->
                <div class="col-lg-6 col-md-12 px-4">

                    <!--First row-->
                    <div class="row">
                        <div class="col-1 mr-3">
                        </div>
                        <div class="col-10">
                            <p class="feature-title">Одно и двух камерные стеклопакеты обладают:
                            </p>
                            <p>Высокими энергосберегающими показателями благодаря теплоизоляционному слою
                                </p>
                            <p>Отличные свойства шумопоглащения обеспечивают прочные стекла шириной 4мм</p>
                        </div>
                    </div>

                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-lg-6 col-md-12">
                    <img src="/img/steklopaket.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
                <!--/Grid column-->

            </div>
            <!--/Grid row-->

        </section>
        <!--Section: Main features & Quick Start-->

        <hr class="my-5">

        <!--Section: More-->
        <section class="mb-5">

            <h2 class="my-5 h3 text-center">С нами легко работать</h2>

            <!--First row-->
            <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">1</p>
                        </div>
                        <div class="col-10 mb-2 pl-3">
                            <h5 class="feature-title font-bold mb-1">Вызов замерщика</h5>
                            <!--<p class="grey-text mt-2">Chrome, Firefox, IE, Safari, Opera, Microsoft Edge - MDB loves all-->
                            <!--browsers; all browsers love MDB.-->
                            <!--</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">2</p>
                        </div>
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Прощет стоимости</h5>
                            <!--<p class="grey-text mt-2">MDB becomes better every month. We love the project and enhance as-->
                            <!--much as possible.-->
                            <!--</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">3</p>
                        </div>
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Подписание договора</h5>
                            <!--<p class="grey-text mt-2">Our society grows day by day. Visit our forum and check how it is-->
                            <!--to be a part of our family.-->
                            <!--</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

            </div>
            <!--/First row-->

            <!--Second row-->
            <div class="row features-small mt-4 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">4</p>
                        </div>
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Предлоплата 70%</h5>
                            <!--<p class="grey-text mt-2">Material Design for Bootstrap comes with both, compiled, ready to-->
                            <!--use libraries including all features as-->
                            <!--well as modules for CSS (SASS files) and JS.</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">5</p>
                        </div>
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Выполнение работ</h5>
                            <!--<p class="grey-text mt-2">We care about reliability. If you have any questions - do not-->
                            <!--hesitate to contact us.-->
                            <!--</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-4 col-lg-6">
                    <!--Grid row-->
                    <div class="row">
                        <div class="col-2">
                            <p class="number">6</p>
                        </div>
                        <div class="col-10 mb-2">
                            <h5 class="feature-title font-bold mb-1">Постоплата и гарантия на установку 1 год</h5>
                            <!--<p class="grey-text mt-2">MDB fully supports Flex Box. You can forget about alignment-->
                            <!--issues.</p>-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </div>
                <!--/Grid column-->

            </div>
            <!--/Second row-->

        </section>
        <!--Section: More-->

        <hr class="my-5">

        <!--Section: Our Jobs-->
        <section class="mb-5" id="our-jobs">

            <h2 class="my-5 h3 text-center">Наши работы</h2>

            <!--First row-->
            <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                <!--Grid column-->
                <div class="col-xl-3 col-sm-6 my-3 text-center">
                    <img src="/img/jobs/job_1.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-3 col-sm-6 my-3 text-center">
                    <img src="/img/jobs/job_2.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-3 col-sm-6 my-3 text-center">
                    <img src="/img/jobs/job_3.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
                <!--/Grid column-->

                <!--Grid column-->
                <div class="col-xl-3 col-sm-6 my-3 text-center">
                    <img src="/img/jobs/job_4.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
                <!--/Grid column-->

            </div>
            <!--/First row-->

        </section>
        <!--Section: More-->

        <hr class="my-5">

        <!--Section: More-->
        <section class="mb-5" id="order">
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('customer.store.public') }}" method="POST">
                        {{ csrf_field() }}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" required placeholder="Имя" id="name" name="name"
                                           class="form-control" value="{{ old('name') }}">
                                    <label for="name" class=""><span class="text-danger">*</span> Имя</label>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="md-form">
                                    <input type="number" required placeholder="071 999 99 99"
                                           id="phone" name="phone" class="form-control"  value="{{ old('phone') }}">
                                    <label for="phone" class=""><span class="text-danger">*</span> Номер
                                        телефона</label>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <!--Basic textarea-->
                                <div class="md-form">
                                    <textarea type="text" name="comment" id="comment" class="form-control md-textarea"
                                              rows="3">{{ old('comment') }}</textarea>
                                    <label for="comment" class="">Комментарий</label>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <span class="text-center d-inline-block">
                                     <button type="submit" class="btn btn-grey waves-effect waves-light">
                                        Отправить
                                    </button>
                                    <p class="small grey-text m-0">
                                        Нажимая на кнопку отправки формы, вы подтверждаете, что прочитали <a href="{{ route('license') }}">пользовательское соглашение</a>, согласны с его положениями и согласны оставить свои данные
                                    </p>
                                </span>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </section>
        <!--Section: More-->

        <script>
            var inputPhone = document.getElementById('phone');
            inputPhone.oninput = function () {
                if (this.value.length > 10) {
                    this.value = this.value.slice(0, 10);
                }
            };

            if (window.innerWidth > window.outerWidth) {
                var furniture = document.querySelectorAll('.furniture');
                furniture.forEach(function (e) {
                    e.className += ' d-none'
                })
            }
        </script>

    </div>
</main>
<!--Main layout-->
@endsection