<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Авторизация</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ URL::asset('css/admin/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/admin/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/admin/vendor.bundle.addons.css') }}">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ URL::asset('css/admin/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ URL::asset('img/shortcut/logo.svg') }}"/>

    <style>
        .status-bar {
            padding: 2px;
            display: block;
            min-width: 20px;
            min-height: 13px;
        }
        .main {
            background-color: #eee;
        }
    </style>

</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="main content-wrapper d-flex align-items-center theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="label">Пользователь</label>
                                <div class="input-group">
                                    <input type="text"  value="{{ old('email') }}" name="email" required class="form-control" placeholder="Пользователь">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label">Пароль</label>
                                <div class="input-group">
                                    <input type="password" value="{{ old('password') }}" name="password" required class="form-control" placeholder="*********">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary submit-btn btn-block">Авторизоваться</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- page-body-wrapper ends -->
</div>

</body>

</html>