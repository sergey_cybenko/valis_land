<!--Section: More-->
<section class="mb-5" id="order">
    <div class="row">
        <div class="col-12">
            <form action="{{ route('customer.store.public') }}" method="POST">
                {{ csrf_field() }}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form">
                            <input type="text" required placeholder="Имя" id="name" name="name"
                                   class="form-control" value="{{ old('name') }}">
                            <label for="name" class=""><span class="text-danger">*</span> Имя</label>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="md-form">
                            <input type="number" required placeholder="071 999 99 99"
                                   id="phone" name="phone" class="form-control"  value="{{ old('phone') }}">
                            <label for="phone" class=""><span class="text-danger">*</span> Номер
                                телефона</label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <!--Basic textarea-->
                        <div class="md-form">
                                    <textarea type="text" name="comment" id="comment" class="form-control md-textarea"
                                              rows="3">{{ old('comment') }}</textarea>
                            <label for="comment" class="">Комментарий</label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                                <span class="text-center d-inline-block">
                                     <button type="submit" class="btn btn-grey waves-effect waves-light">
                                        Отправить
                                    </button>
                                    <p class="small grey-text m-0">
                                        Нажимая на кнопку отправки формы, вы подтверждаете, что прочитали <a href="{{ route('license') }}">пользовательское соглашение</a>, согласны с его положениями и согласны оставить свои данные
                                    </p>
                                </span>
                    </div>
                </div>

            </form>
        </div>
    </div>


</section>
<!--Section: More-->