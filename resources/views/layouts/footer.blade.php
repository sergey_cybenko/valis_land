
<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

    <!--Copyright-->
    <div class="footer-copyright py-3">
        <div class="row">
            <div class="col-6">
                <a href="{{ route('license') }}" target="_blank">Пользовательское соглашение</a>
            </div>
            <div class="col-6">
                <a href="{{ env('LINK_VK') }}" target="_blank">
                    <i class="fab fa-vk mr-3"></i>
                </a>
            </div>
        </div>

    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ URL::asset('js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/mdb.min.js') }}"></script>
<div class="hiddendiv common"></div>
<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>