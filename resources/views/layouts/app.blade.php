<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

        <!-- Brand -->
        <a class="navbar-brand" href="/">
            <img src="/img/shortcut/logo.svg"
                alt="Логотип сата по продаже окон" class="z-depth-0 img-fluid"
                style="max-width: 80px">
        </a>

        <!-- Links -->
        <div>
            <!-- Right -->
            <ul class="navbar-nav ml-auto">
                @include('layouts.phones')
            </ul>

        </div>

    </div>
</nav>
<!-- Navbar -->

@yield('content')
@include('layouts.footer')

</body>
<div style="top: 0px; left: 696.5px;"></div>
</html>