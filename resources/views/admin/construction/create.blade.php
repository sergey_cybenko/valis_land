@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Конструкция</h3>
                            <form class="form-sample" method="POST" action="{{ route('construction.store') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Название конструкции</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Описание конструкции</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="description" value="{{ old('description') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Ссылка на изображение</label>
                                            <div class="col-sm-9">
                                                <input type="file" name="image" class="btn-sm btn-light form-control-file" value="{{ old('image')}}">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection
