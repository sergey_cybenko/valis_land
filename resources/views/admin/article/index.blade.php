@extends('admin.dashboard.app')

@section('content')

<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Продукты</h3>
                        <a href="{{ route('article.create') }}" class="btn btn-sm btn-primary text-white">Создать</a>
                        <div class="table-responsive my-5">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        Заголовок
                                    </th>
                                    <th>
                                        Показывать
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($collection as $model)
                                    <tr>
                                        <td class="py-1">
                                            {{ $model->id }}
                                        </td>
                                        <td>
                                            <a href="{{ route('post', ['slug' => $model->slug]) }}">{{ $model->title }}</a>
                                        </td>
                                        <td>
                                            @if( $model->show )
                                                +
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: right; ">
                                            <form class="d-inline" action="{{ route('article.destroy', ['slug' => $model->slug]) }}" method="POST">
                                                {{method_field('DELETE')}}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm btn-danger">Удалить</button>
                                            </form>
                                            <a href="{{ route('article.show', ['slug' => $model->slug]) }}" class="btn btn-sm btn-primary color-white">Изменить</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                        <div class="d-flex justify-content-center">
                            {{ $collection->links('layouts.pagination') }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->

</div>
<!-- main-panel ends -->

@endsection