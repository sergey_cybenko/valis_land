@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-sample" method="POST" action="{{ route('article.update', ['slug' => $model->slug]) }}">
                                {{ csrf_field() }}
                                {{method_field('PUT')}}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Заголовок</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="{{ $model->title }}" name="title" data-name="title" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Description</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" data-name="description" rows="6" class="form-control">{{ $model->description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Keywords</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="{{ $model->keywords }}" name="keywords" data-name="keywords" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Краткое описание</label>
                                            <div class="col-sm-9">
                                                <textarea name="thumbnail" data-name="thumbnail" rows="6" class="form-control">{{ $model->thumbnail }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">
                                                Показывать
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="hidden" name="content" id='content' value="{{ $model->content }}">
                                                <input type="hidden" name="show" data-name="show" value="0">
                                                <input type="hidden" name="show" value="0">
                                                @if($model->show)
                                                    <input type="checkbox" checked name="show" value="1" class="form-check-input">
                                                @else
                                                    <input type="checkbox" name="show" value="1" class="form-check-input">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col">
                                        <button type="submit" id="btn-submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>

                            <div id="editor" style="height: 500px;">
                                {!! $model->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- main-panel ends -->

@endsection

@section('footer')
    <!-- Main Quill library -->
    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>

    <script src="{{ URL::asset('js/admin/quill.js') }}"></script>
    <script src="{{ URL::asset('js/admin/quill.min.js') }}"></script>

    <!-- Theme included stylesheets -->
    <link href="{{ URL::asset('css/admin/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/admin/quill.bubble.css') }}" rel="stylesheet">

    <!-- Core build with no theme, formatting, non-essential modules -->
    <link href="{{ URL::asset('css/admin/quill.core.css') }}" rel="stylesheet">
    {{--<script src="{{ URL::asset('js/admin/quill.core.js') }}"></script>--}}

    <script>

        var quill = new Quill('#editor', {
            modules: {
                'syntax': true,
                'toolbar': [
                    [{ 'size': [] }],
                    [ 'bold', 'italic', 'underline', 'strike' ],
                    [{ 'color': [] }, { 'background': [] }],
                    [{ 'script': 'super' }, { 'script': 'sub' }],
                    [{ 'header': '2' }, 'blockquote'],
                    [{ 'list': 'ordered' }, { 'list': 'bullet'}, { 'indent': '-1' }, { 'indent': '+1' }],
                    [ 'direction', { 'align': [] }],
                    [ 'link', 'formula' ]
                ],
                history: {
                    delay: 2000,
                    maxStack: 500,
                    userOnly: true
                }
            },
            placeholder: 'Compose an epic...',
            theme: 'snow'  // or 'bubble'
        });

        document.addEventListener("mousemove", function () {
            jQuery('#content').val(document.querySelector('#editor').children[0].innerHTML)
        });

        document.addEventListener("keyup", function () {
            jQuery('#content').val(document.querySelector('#editor').children[0].innerHTML)
        });
    </script>
@endsection