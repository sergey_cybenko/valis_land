@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Фурнтура</h3>
                            <form class="form-sample" method="POST" action="{{ route('product.update', ['id' => $product->id]) }}">
                                {{method_field('PUT')}}
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Заголовок продукта</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="title" value="{{ $product->title }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Описание продукта</label>
                                            <div class="col-sm-9">
                                                <textarea rows="6" type="text" name="description" class="form-control">{{ $product->description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Цена</label>
                                            <div class="col-sm-9">
                                                <input type="number" name="price" value="{{ $product->price }}"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">
                                                Показывать
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="hidden" name="show" value="0">
                                                @if($product->show)
                                                    <input type="checkbox" checked name="show" value="1" class="form-check-input">
                                                @else
                                                    <input type="checkbox" name="show" value="1" class="form-check-input">
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Конструкция</label>
                                            <div class="col-sm-9">
                                                <select name="construction" class="form-control">
                                                    @foreach($constructions as $model)
                                                        @if($model->id == $product->construction_id)
                                                            <option selected="selected" value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Профиль</label>
                                            <div class="col-sm-9">
                                                <select name="profile" class="form-control">
                                                    @foreach($profiles as $model)
                                                        @if($model->id == $product->profile_id)
                                                            <option selected="selected"
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Фурнитура</label>
                                            <div class="col-sm-9">
                                                <select name="furniture" class="form-control">
                                                    @foreach($furnitures as $model)
                                                        @if($model->id == $product->furniture_id)
                                                            <option selected="selected"
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Стеклопакет</label>
                                            <div class="col-sm-9">
                                                <select name="glass" class="form-control">
                                                    @foreach($glasses as $model)
                                                        @if($model->id == $product->glass_id)
                                                            <option selected="selected"
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection
