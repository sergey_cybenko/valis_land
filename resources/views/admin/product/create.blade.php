@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Создать продукт</h3>
                            <form class="form-sample" method="POST" action="{{ route('product.store') }}">
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Заголовок продукта</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="title" value="{{ old('title') }}"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Описание продукта</label>
                                            <div class="col-sm-9">
                                                <textarea rows="6" name="description" class="form-control">{{ old('description') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Цена</label>
                                            <div class="col-sm-9">
                                                <input type="number" name="price" value="{{ old('price') }}"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">
                                                Показывать
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="hidden" name="show" value="0">
                                                @if(null != old('show'))
                                                    @if(old('show'))
                                                        <input type="checkbox" checked name="show" value="1" class="form-check-input">
                                                    @else
                                                        <input type="checkbox" name="show" value="1" class="form-check-input">
                                                    @endif
                                                @else
                                                    <input type="checkbox" checked name="show" value="1" class="form-check-input">
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Конструкция</label>
                                            <div class="col-sm-9">
                                                <select name="construction" class="form-control">
                                                    @foreach($constructions as $model)
                                                        <option value="{{ $model->id }}">
                                                            {{ $model->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Профиль</label>
                                            <div class="col-sm-9">
                                                <select name="profile" class="form-control">
                                                    @foreach($profiles as $model)
                                                        <option value="{{ $model->id }}">
                                                            {{ $model->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Фурнитура</label>
                                            <div class="col-sm-9">
                                                <select name="furniture" class="form-control">
                                                    @foreach($furnitures as $model)
                                                        <option value="{{ $model->id }}">
                                                            {{ $model->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Стеклопакет</label>
                                            <div class="col-sm-9">
                                                <select name="glass" class="form-control">
                                                    @foreach($glasses as $model)
                                                        <option value="{{ $model->id }}">
                                                            {{ $model->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection
