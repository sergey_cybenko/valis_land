@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Horizontal Two column</h3>
                            <form class="form-sample" action="{{ route('customer.store') }}" method="POST">
                                {{ csrf_field() }}
                                <p class="card-description">
                                    Personal info
                                </p>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Имя</label>
                                            <div class="col-sm-9">
                                                <input value="{{ old('name') }}" type="text" name="name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Номер телефона</label>
                                            <div class="col-sm-9">
                                                <input value="{{ old('phone') }}" type="text" name="phone" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Комментарий</label>
                                            <div class="col-sm-9">
                                                <textarea type="text" name="comment" class="form-control">{{ old('comment') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Статус</label>
                                            <div class="col-sm-9">
                                                <select name="status" class="form-control">
                                                    @foreach($statuses as $status)
                                                        <option style="background-color: #{{ $status->color }}" value="{{ $status->id }}">
                                                            {{ $status->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection

@section('footer')
    <script>
        var inputPhone = document.getElementById('phone');
        inputPhone.oninput = function () {
            if (this.value.length > 10) {
                this.value = this.value.slice(0, 10);
            }
        };
    </script>
@endsection