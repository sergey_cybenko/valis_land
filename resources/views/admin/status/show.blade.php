@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Статус клиента</h3>
                            <form class="form-sample" method="POST" action="{{ route('status.update', ['id' => $model->id]) }}">
                                {{method_field('PUT')}}
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Название статуса</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" value="{{ $model->name }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div id="container" >
                                                    
                                            <div id="picker" class="block">
                                                            
                                                <div class="ui-color-picker" data-topic="picker"
                                                     data-mode="HSL"></div>
                                                            
                                                <div id="picker-samples" sample-id="master"></div>
                                                            
                                                <div id="controls">
                                                                    
                                                    <div id="delete">
                                                                            
                                                        <div id="trash-can"></div>
                                                                        
                                                    </div>
                                                                     
                                                    <div id="void-sample" class="icon-palette"></div>
                                                                
                                                </div>
                                                        
                                            </div>
                                                
                                        </div>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Обновить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection


@section('footer')
    <link rel="stylesheet" href="{{ URL::asset('css/admin/color-picker.css') }}">
    <script src="{{ URL::asset('js/admin/color-picker.js') }}"></script>

    <script>
        const picker = new ColorPicker({
            dom: document.getElementById('picker'),
            value: '#{{ $model->color }}'
        });

    </script>
@endsection