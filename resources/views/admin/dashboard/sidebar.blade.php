<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Клинеты</span>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-article" aria-expanded="false" aria-controls="ui-article">
                <span class="menu-title">Статьи</span>
            </a>
            <div class="collapse" id="ui-article">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('article.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('article.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-product" aria-expanded="false" aria-controls="ui-product">
                <span class="menu-title">Продукт</span>
            </a>
            <div class="collapse" id="ui-product">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-construction" aria-expanded="false" aria-controls="ui-construction">
                <span class="menu-title">Конструкция</span>
            </a>
            <div class="collapse" id="ui-construction">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('construction.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('construction.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-furniture" aria-expanded="false" aria-controls="ui-furniture">
                <span class="menu-title">Фурнитура</span>
            </a>
            <div class="collapse" id="ui-furniture">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('furniture.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('furniture.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-glass" aria-expanded="false" aria-controls="ui-glass">
                <span class="menu-title">Стеклопакеты</span>
            </a>
            <div class="collapse" id="ui-glass">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('glass.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('glass.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-profile" aria-expanded="false" aria-controls="ui-profile">
                <span class="menu-title">Профиль</span>
            </a>
            <div class="collapse" id="ui-profile">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic-status" aria-expanded="false" aria-controls="ui-basic-status">
                <span class="menu-title">Статус клиентов</span>
            </a>
            <div class="collapse" id="ui-basic-status">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('status.index') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('status.create') }}">Создать</a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</nav>