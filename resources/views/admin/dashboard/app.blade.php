<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Star Admin Free Bootstrap Admin Dashboard Template</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ URL::asset('css/admin/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/admin/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/admin/vendor.bundle.addons.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ URL::asset('css/admin/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ URL::asset('img/shortcut/logo.svg') }}" />

    @yield('head')

    <style>
        .status-bar {
            padding: 2px;
            display: block;
            min-width: 20px;
            min-height: 13px;
        }
    </style>

</head>

<body>
<div class="container-scroller">
    @include('admin.dashboard.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        @include('admin.dashboard.sidebar')

@yield('content')

    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{ URL::asset('js/admin/vendor.bundle.base.js') }}"></script>
<script src="{{ URL::asset('js/admin/vendor.bundle.addons.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ URL::asset('js/admin/off-canvas.js') }}"></script>
<script src="{{ URL::asset('js/admin/misc.js') }}"></script>
<!-- endinject -->

@yield('footer')

<script>
    (function ($) {
        $.ajax({
            url: '{{ route('count.customer.default') }}',
            success: function(content){
                document.querySelector('#customers-count')
                    .innerText = content
            }
        });
    })(jQuery)
</script>

</body>

</html>