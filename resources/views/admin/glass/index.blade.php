@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Стеклопакет</h3>
                            <a href="{{ route('glass.create') }}" class="btn btn-sm btn-primary text-white">Создать</a>
                            <div class="table-responsive my-5">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            id
                                        </th>
                                        <th>
                                            Название
                                        </th>
                                        <th>
                                            Описание
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($collection as $model)
                                            <tr>
                                                <td class="py-1">
                                                    {{ $model->id }}
                                                </td>
                                                <td>
                                                    {{ $model->name }}
                                                </td>
                                                <td>
                                                    {{ $model->description }}
                                                </td>
                                                <td style="text-align: right; ">
                                                    <form class="d-inline" action="{{ route('glass.destroy', ['id' => $model->id]) }}" method="POST">
                                                        {{method_field('DELETE')}}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-sm btn-danger">Удалить</button>
                                                    </form>
                                                    <a href="{{ route('glass.show', ['id' => $model->id]) }}" class="btn btn-sm btn-primary color-white">Изменить</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>


                            <div class="d-flex justify-content-center">
                                {{ $collection->links('layouts.pagination') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection