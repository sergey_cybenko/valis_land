@extends('admin.dashboard.app')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Профиль</h3>
                            <form class="form-sample" method="POST" action="{{ route('profile.update', ['id' => $profile->id]) }}">
                                {{method_field('PUT')}}
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Название профиля</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" value="{{ $profile->name }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Описание профиля</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="description" value="{{ $profile->description }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Фурнитура</label>
                                            <div class="col-sm-9">
                                                <select multiple="multiple" name="furnitures[]" class="form-control">
                                                    @foreach($furnitures as $model)
                                                        @if(in_array($model->id, $profile->furnitures->pluck('id')->all()))
                                                            <option selected="selected"
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Стеклопакет</label>
                                            <div class="col-sm-9">
                                                <select multiple="multiple" name="glasses[]" class="form-control">
                                                    @foreach($glasses as $model)
                                                        @if(in_array($model->id, $profile->glasses->pluck('id')->all()))
                                                            <option selected="selected"
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @else
                                                            <option
                                                                    value="{{ $model->id }}">
                                                                {{ $model->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('admin.dashboard.footer')
    </div>
    <!-- main-panel ends -->

@endsection
