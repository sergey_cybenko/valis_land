@extends('layouts.nav_app')
@section('content')
    <!-- data-ride="carousel" -->
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade"
         style="
            max-height: 35%;
            min-height: 400px;
        ">

        <!--Slides-->
        <div class="carousel-inner" role="listbox">

            <!--First slide-->
            <div class="carousel-item active" style="
            background-image: url(/img/success.jpg);
            background-size: cover;
        ">
                <div class="view">

                    <!-- Mask & flexbox options-->
                    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

                        <!-- Content -->
                        <div class="text-center white-text mx-5 wow fadeIn pt-5"
                             style="visibility: visible; animation-name: fadeIn;">
                            <h1 class="h2 mb-4">
                                <strong>Наш менеджер перезвонит вам в ближайшее время</strong>
                            </h1>

                            <a href="#our_jobs"
                               class="btn btn-outline-white btn-lg waves-effect waves-light">Наши работы
                            </a>
                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Mask & flexbox options-->

                </div>
            </div>
            <!--/First slide-->

        </div>
        <!--/.Slides-->

    </div>
    <!--/.Carousel Wrapper-->

    <main>
        <div class="container" id="our_jobs">
            <!--Section: Our Jobs-->
            <section class="mb-5" id="our-jobs">

                <h3 class="my-5 h3 text-center">Окна и балконы</h3>

                <!--First row-->
                <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/job_1.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/job_2.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/job_3.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/job_4.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                </div>
                <!--/First row-->

                <h3 class="my-5 h3 text-center">Ремонт</h3>

                <!--First row-->
                <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/repair/job_1.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/repair/job_2.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/repair/job_3.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/repair/job_4.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                </div>
                <!--/First row-->

                <h3 class="my-5 h3 text-center">Строительство</h3>

                <!--First row-->
                <div class="row features-small mt-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/build/job_1.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/build/job_2.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/build/job_3.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                    <!--Grid column-->
                    <div class="col-xl-3 col-sm-6 my-3 text-center">
                        <img src="/img/jobs/build/job_4.jpg" class="img-fluid z-depth-1-half" alt="">
                    </div>
                    <!--/Grid column-->

                </div>
                <!--/First row-->

            </section>
            <!--Section: More-->
        </div>
    </main>

@endsection