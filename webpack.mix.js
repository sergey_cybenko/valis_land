const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// copy('resources/css', 'public/css');
// copy('resources/js', 'public/js');

copy([
    'node_modules/quill/dist/quill.js',
    'node_modules/quill/dist/quill.min.js',
    'node_modules/quill/dist/quill.core.js',
], 'public/js/admin/quill.js');

copy([
    'node_modules/quill/dist/quill.snow.css',
    'node_modules/quill/dist/quill.bubble.css',
    'node_modules/quill/dist/quill.core.css',
], 'public/css/admin/quill.css');